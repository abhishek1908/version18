package com.abhijeet.abhishek.version_18;

import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.abhijeet.abhishek.version_18.helper.VolleySingleton;
import com.abhijeet.abhishek.version_18.model.Tshirt;
import com.abhijeet.abhishek.version_18.other.URLs;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.journeyapps.barcodescanner.BarcodeEncoder;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class TshirtRegisterActivity extends AppCompatActivity {

    private ProgressDialog pDialog;
    private final static String TAG = "TshirtRegisterActivity";
    private EditText editTextEmail;
    private EditText sname;
    private RadioButton radioButton;
    private RadioGroup radioGroup;
    private Button btnRegister;
    private ImageView imageViewQr;
    private Toolbar toolbar;
    private String email,gender,size,name;
    private Tshirt tshirt;
    private Spinner tshirtSizeSpinner;
    private HashMap<Integer,String> mp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tshirt_register);
        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);
        toolbar= (Toolbar)findViewById(R.id.toolbar_tshirtegister);
        toolbar.setTitle("Register T-Shirt");
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp);
        editTextEmail = findViewById(R.id.editTextEmail);
        sname=findViewById(R.id.s_name);
        btnRegister = findViewById(R.id.btnRegister);
        imageViewQr = findViewById(R.id.imageViewQr);
        radioGroup = findViewById(R.id.radiogrouptshirt);
        tshirtSizeSpinner = findViewById(R.id.tshirtSizeSpinner);

        mp = new HashMap<Integer, String>();
        //add element to hashmap
        mp.put(1,"S");
        mp.put(2,"M");
        mp.put(3,"L");
        mp.put(4,"XL");
        mp.put(5,"XXL");

        //Spinner drop down elements
        List<String> tshirtSizeList = new ArrayList<String>();
        tshirtSizeList.add(mp.get(1));
        tshirtSizeList.add(mp.get(2));
        tshirtSizeList.add(mp.get(3));
        tshirtSizeList.add(mp.get(4));
        tshirtSizeList.add(mp.get(5));
        //Creating adapter for spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_dropdown_item,tshirtSizeList);
        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        //attaching data adapter to spinner
        tshirtSizeSpinner.setAdapter(dataAdapter);
        size = mp.get(1);
        tshirtSizeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                ((TextView)parent.getChildAt(0)).setTextColor(0xFF3AA59B);
                size = parent.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        final LinearLayout ll1= findViewById(R.id.qrregister);
        final LinearLayout qr= findViewById(R.id.qrlayout);
        qr.setVisibility(View.INVISIBLE);
        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                radioButton = (RadioButton) findViewById(radioGroup.getCheckedRadioButtonId());
                email = editTextEmail.getText().toString().trim();
                gender = radioButton.getText().toString();
                name = sname.getText().toString().trim();
                if(TextUtils.isEmpty(name)){
                    sname.setError("Please enter your name");
                    sname.requestFocus();
                    return;
                }
                if(TextUtils.isEmpty(email)){
                    editTextEmail.setError("Please enter your email");
                    editTextEmail.requestFocus();
                    return;
                }
                if(!isValidEmail(email)){
                    editTextEmail.setError("Please enter valid email id");
                    editTextEmail.requestFocus();
                    return;
                }
                pDialog.setMessage("Generating QR ...");
                showDialog();
                StringRequest stringRequest = new StringRequest(Request.Method.POST, URLs.URL_TSHIRTREGISTER,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                    hideDialog();
                                try {
                                    JSONObject jsonObject = new JSONObject(response);
                                    if (!jsonObject.getBoolean("error")) {
                                        Toast.makeText(getApplicationContext(), jsonObject.getString("message"),
                                        Toast.LENGTH_SHORT).show();
                                        ll1.setVisibility(View.INVISIBLE);
                                        String tshirt_details = jsonObject.getString("user_tshirt");
                                        MultiFormatWriter multiFormatWriter = new MultiFormatWriter();
                                        try {
                                            BitMatrix bitMatrix = multiFormatWriter.encode(
                                            tshirt_details, BarcodeFormat.QR_CODE,500,500);
                                            BarcodeEncoder barcodeEncoder = new BarcodeEncoder();
                                            Bitmap bitmap = barcodeEncoder.createBitmap(bitMatrix);
                                            imageViewQr.setImageBitmap(bitmap);
                                            qr.setVisibility(v.VISIBLE);
                                        }catch (WriterException e){
                                            e.printStackTrace();
                                        }

                                    } else {
                                        Toast.makeText(getApplicationContext(), jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                hideDialog();
                                Toast.makeText(getApplicationContext(),  "Server is not Responding", Toast.LENGTH_SHORT).show();
                            }

                        }) {
                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {
                        Map<String, String> params = new HashMap<>();
                        params.put("email", email);
                        params.put("name", name);
                        params.put("gender", gender);
                        params.put("size", size);

                        return params;
                    }
                };
                VolleySingleton.getInstance(TshirtRegisterActivity.this).addToRequestQueue(stringRequest);

            }
        });

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private boolean isValidEmail(String email) {
        return Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }
    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }
}
