package com.abhijeet.abhishek.version_18.other;

public class URLs {
    private static final String ROOT_URL = "https://api.version18.in/app_request.php?apicall=";
    public static final String URL_REGISTER = ROOT_URL + "signup";
    public static final String URL_LOGIN= ROOT_URL + "login";
    public static final String URL_TSHIRTREGISTER = "https://api.version18.in/tshirtreg.php?apicall=" + "tshirt";
}

