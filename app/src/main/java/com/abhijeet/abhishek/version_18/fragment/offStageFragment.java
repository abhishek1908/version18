package com.abhijeet.abhishek.version_18.fragment;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.abhijeet.abhishek.version_18.R;

public class offStageFragment extends Fragment implements View.OnClickListener{
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public offStageFragment() {
        // Required empty public constructor
    }
    public static offStageFragment newInstance(String param1, String param2) {
        offStageFragment fragment = new offStageFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v=inflater.inflate(R.layout.fragment_off_stage, container, false);
        LinearLayout prix=(LinearLayout)v.findViewById(R.id.codeprixlayout);
        LinearLayout block=(LinearLayout)v.findViewById(R.id.mindblocklayout);
        LinearLayout web=(LinearLayout)v.findViewById(R.id.weblayout);
        LinearLayout sequal=(LinearLayout)v.findViewById(R.id.sqllayout);
        LinearLayout games=(LinearLayout)v.findViewById(R.id.gameslayout);
        LinearLayout relay=(LinearLayout)v.findViewById(R.id.coderelaylayout);
        LinearLayout bit=(LinearLayout)v.findViewById(R.id.bitlayout);
        prix.setOnClickListener(this);
        relay.setOnClickListener(this);
        block.setOnClickListener(this);
        web.setOnClickListener(this);
        sequal.setOnClickListener(this);
        games.setOnClickListener(this);
        bit.setOnClickListener(this);
        return v;
    }

    @Override
    public void onClick(View v) {
        Fragment des=new eventDescriptionFragment();
        Bundle bundle=new Bundle();
        switch (v.getId()){
            case R.id.codeprixlayout:
                bundle.putString("logo","codeprix");
                bundle.putString("ename","CodePrix");
                bundle.putString("desc","Speed defines everything even your chance of winning this event. Codeprix is a form of coding competition where speed and accuracy matters the most. Slow and Steady is not the recipe for success you have to be fast and energetic. In the competition each participating team will be given one question at a time and once the question is completely submitted by any team then the question gets locked for everyone and the next question will be given . The one with maximum points at the end will be declared as the winner.");
                bundle.putString("tagline","Decode it before your opponent does");
                bundle.putString("rules","\n\nPrelims will be a qualifier to get into finals. It will consist of some" +
                        "technical questions and some aptitude " +
                        "questions. It will be a 30 minute round.\n" +
                        "* Only one team per college will be permitted.\n" +
                        "* A team will consist of at most two members.\n" +
                        "\n"+
                        "The final will be a coding contest. This round will be of 1 hour " +
                        "comprising specific number of questions.\n" +
                        "* Time for every question is at max 15 minutes .\n" +
                        "* For every wrong submission marks will be deducted from total.\n" +
                        "* The leading team will be declared as the " +
                        "winner.\n" +
                        "* If case of any malpractices, the respective team will be " +
                        "disqualified from the event.\n" +
                        "* The decision of the organising committee, Version’18 will be " +
                        "final and binding.\n" +
                        "*In Case of a tie the preference will be given to the team who scored higher in prelims.\n\n\n");
                des.setArguments(bundle);

                break;
            case R.id.coderelaylayout:
                bundle.putString("logo","coderelay");
                bundle.putString("ename","CodeRelay");
                bundle.putString("desc","We believe 'Talent wins game but teamwork wins championship' so, to enhance your team spirit and examine your coordination we are here with this team event named 'Code Relay'. Cracking the code problem alone is easy but the talent is when you understand your teammate's code and contribute your mind into it to win the battle. So, if you aim above the mark, come and grab this opportunity to showcase your hidden talent.");
                bundle.putString("tagline","Strategies for success");
                bundle.putString("rules","\n\nPrelims: \n\n"+
                        "* It will consist of some technical questions and " +
                        "some aptitude questions.\n" +
                        "* It will be a 30 minute round.\n" +
                        "* Only one team per college will be permitted.\n" +
                        "* A team can have any number of members for Prelims.\n" +
                        "\nFinals \n\n"+
                        "* The final will be a coding contest.\n" +
                        "* This round will be of 45 minutes comprising specific " +
                        "number of questions.\n" +
                        "* A team must have exact 3 members." +
                        "* After every 15 minutes, the " +
                        "members will be shuffled and the next member needs to continue the contest.\n"+
                        "* An individual member can code at a particular time.\n" +
                        "* The team leading the leader board will be declared as the winner.\n" +
                        "* If case of any malpractices, the respective team will be disqualified from the event.\n" +
                        "* The decision of the organising committee, Version’18 will be final and binding.\n\n\n");
                des.setArguments(bundle);

                break;
            case R.id.mindblocklayout:
                bundle.putString("logo","mindblock");
                bundle.putString("ename","MindBlock");
                bundle.putString("desc","Great stories are the cornerstone of every great event, but harnessing their true potential is often overlooked in the planning phase. Coding an algorithm is easy but cracking a code from a story is a mystery. If your teammate is complementary and not contradictory then Mindblock is the platform to create momentum and win the contest. So think about how you can combine your potential with your teammate's intellect by telling a story in a unique way.");
                bundle.putString("tagline","Untangling the mystery");
                bundle.putString("rules","\n\nPrelims:\n\n" +
                        "* It will consist of questions which will be sent to the registered email id. \n" +
                        "* 35 minutes will be given to the participants to solve the questions.\n\n" +
                        "Finals:\n\n" +
                        "* The participants will be given puzzled questions and they must narrate it to another teammate.\n" +
                        "* The other team mate must interpret and solve the questions.\n" +
                        "* In case of tie then preference will be given to whom who scored higher in prelims.\n" +
                        "* Violation of any rule mentioned above can lead to disqualification at any point of time.\n" +
                        "* The decision of the organising committee, Version’18 will be final and binding.\n\n\n");
                des.setArguments(bundle);

                break;
            case R.id.weblayout:
                bundle.putString("logo","concevoir");
                bundle.putString("ename","Concevoir Le Web");
                bundle.putString("desc","Creativity is a process not an event. Concevoir Le Web is an event to nurture your creative ideas and give them a platform to be showcased upon. And when Designing meets Coding, together they create great works. Colours, themes, images, texts and innovative ideas are the backbone of this event. So, if you carry all these come forward and harness your creativity.");
                bundle.putString("tagline","Creativity beyond imagination");
                bundle.putString("rules","\n\n* Maximum 2 members per group.\n" +
                        "* Participants need to design a 2+ pages website based on the competition topic.\n" +
                        "* Students have to design a web-site on any one of the specified topics and present it to the " +
                        "judges. Decision of the judges will be final.\n" +
                        "* First page must be your Homepage (Containing LOGO/picture depicting the topic and the " +
                        "website name).\n" +
                        "* Three pages must be dedicated to your discussion suitable for the website.\n" +
                        "* The last page must be Information Hub that provides details about: Topics will be provide at the time of event\n" +
                        "* Use of USB drive is strictly prohibited\n\n\n");
                des.setArguments(bundle);

                break;
            case R.id.sqllayout:
                bundle.putString("logo","sequelize");
                bundle.putString("ename","Sequelize");
                bundle.putString("desc","SELECT, JOIN and VIEW. Are these keywords your favorite? If big databases don't scare you and database management brings out the best in you then you are up for a treat. Because we bring you an event where you will be tested on database concepts and SQL. Quench your thirst by querying through two rounds in order to be crowned as champion. So, expect the unexpected, gear up with your armour and brace up for DBMS.");
                bundle.putString("tagline","Be a data digger");
                bundle.putString("rules","\n\nPrelims :\n\n" +
                        "* There will be an online test in this round.\n" +
                        "* There will be 25 questions in the test.\n" +
                        "* Questions will of database concepts (Example Joins,Normalization, DDL, DML etc.).\n" +
                        "* For each correct answer, the score will increase by 4 marks.\n" +
                        "* For each wrong answer, the score will decrease by 1 mark.\n" +
                        "* Test duration will be 30 minutes.\n\n" +
                        "Final Round:\n\n"+
                        "* In this round, participants will be provided the database.\n" +
                        "* There will be 8 Coding (SQL query) questions.\n" +
                        "* Oracle and MYSQL database will be allowed.\n" +
                        "* Questions will in three level easy, medium and hard.\n" +
                        "* Three questions, easy level(max score 3x20=60).\n" +
                        "* Three questions, medium level(max score 3x40=120).\n"+
                        "* Two questions, hard level(max score 2x60=120).\n" +
                        "* Duration of this round will be 45 minutes.\n\n\n");
                des.setArguments(bundle);
                break;
            case R.id.gameslayout:
                bundle.putString("logo","gamerseden_new");
                bundle.putString("ename","Gamers Eden");
                bundle.putString("desc","I don't need to get a life, I'm a gamer, I have lots of lives!'. Version'18 welcomes you to the ultimate gaming arena! Escape the reality and land into the virtual world of survival. Team up! Push yourself farther than you have ever before to see the face of your next enemies. You have to face the bests in the business and then win to be crowned as the victor.");
                bundle.putString("tagline","New dimension of battle");
                bundle.putString("rules","\n\nGeneral Rules:\n\n" +
                        "* These rules apply to everyone attending. They can be " +
                        "changed without notice.\n" +
                        "* You are responsible for your actions and equipment. " +
                        "We are not liable for anything that happens to you " +
                        "and your equipment.\n" +
                        "* Do not attempt to steal items from the event or " +
                        "anything else, legal consequences will be there.\n" +
                        "*  Cheating is an offence. Doing so may lead to " +
                        "disqualification.\n" +
                        "* Listen to those in charge.\n" +
                        "*  We reserve the right to remove anyone from premises " +
                        "for any reason we fit."+
                        "\n\nFurther rules are on website.\n\n\n");
                des.setArguments(bundle);

                break;
            case R.id.bitlayout:
                bundle.putString("logo","frame");
                bundle.putString("ename","BitSpin");
                bundle.putString("desc","Surprise");
                bundle.putString("tagline","Your chance to try?");
                bundle.putString("rules","At the time of Event.");
                des.setArguments(bundle);
        }
        FragmentTransaction t = getFragmentManager().beginTransaction();
        t.replace(R.id.frame,des);
        t.addToBackStack("events");
        t.commit();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
