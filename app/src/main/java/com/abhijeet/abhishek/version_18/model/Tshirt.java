package com.abhijeet.abhishek.version_18.model;

/**
 * Created by jeet on 8/19/2018.
 */

public class Tshirt {
    private String email,gender,size,name;

    public Tshirt(String email,String name,String gender,String size){
        this.email = email;
        this.name=name;
        this.gender = gender;
        this.size = size;
    }
    public String getEmail() {
        return email;
    }

    public String getGender() {
        return gender;
    }

    public String getSize() {
        return size;
    }

    public String getName() {return name;}
}
