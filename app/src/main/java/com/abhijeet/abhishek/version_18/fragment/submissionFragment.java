package com.abhijeet.abhishek.version_18.fragment;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.abhijeet.abhishek.version_18.R;
public class submissionFragment extends Fragment implements View.OnClickListener{
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public submissionFragment() {
        // Required empty public constructor
    }
    public static submissionFragment newInstance(String param1, String param2) {
        submissionFragment fragment = new submissionFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
       View v= inflater.inflate(R.layout.fragment_submission, container, false);
        LinearLayout scib=(LinearLayout)v.findViewById(R.id.scriblelayout);
        LinearLayout photo=(LinearLayout)v.findViewById(R.id.photolayout);
        scib.setOnClickListener(this);
        photo.setOnClickListener(this);
        return v;
    }

    @Override
    public void onClick(View v) {
        Fragment des=new eventDescriptionFragment();
        Bundle bundle=new Bundle();
        switch (v.getId()){
            case R.id.scriblelayout:
                bundle.putString("logo","scribble");
                bundle.putString("ename","Scribble");
                bundle.putString("desc","Ever sat in a class or lecture and found your pen wandering across your notebook and making a series of patterns? Creativity involves breaking out of established  patterns in order  to look at things in a different way. Scribble is the event to discover and unleash the creativity in you through a doodling masterpiece that comes from a conflict of ideas.");
                bundle.putString("tagline","Unleash the artist");
                bundle.putString("rules","\n\n* Participant has to make a doodle on the word “Qubykon” and the theme will be innovation.\n" +
                        "* One doodle per participant is allowed. In case of more than one entries from the same " +
                        "participant then the first entry to arrive will be taken as an entry in the contest and " +
                        "subsequent entries will be disqualified.\n" +
                        "* Doodle can be either handmade or using Photoshop but the one who will use Photoshop will " +
                        "be given priority in choosing winners\n" +
                        "* You can add a short description of your doodling.\n" +
                        "* Submissions are starting from 17th August.\n\n" +
                        "Submission:\n\n" +
                        "* Submit your entry based on the given theme along with following details before 31st August " +
                        "till 12 am (midnight).\n" +
                        "* Name of your college\n" +
                        "* Contact number\n" +
                        "* Email id ( By which you have registered for version)\n" +
                        "* Submit your entries at ppcmca2018@gmail.com\n" +
                        "* If you are using Photoshop, you have to send a PSD file of that. \n\n\n");
                des.setArguments(bundle);
                break;
            case R.id.photolayout:
                bundle.putString("logo","photography");
                bundle.putString("ename","Pixelate");
                bundle.putString("desc","Pixelate is a photography event to showcase your talent. For some people it is a hobby and for some it is their passion. This event gives you an opporunity to separate yourself from the mundane and show your artistry.");
                bundle.putString("tagline","Make it RAW");
                bundle.putString("rules","\n\n* Participants will be provided with two themes. Best photo will be declared as winner. " +
                        " Photos must be clicked after 14th August.\n" +
                        "* Photos must be on manual mode and clicked on manual focus mode.\n" +
                        "* Exposure triangle criteria must be followed. " +
                        " Only .raw and .jpg formats are allowed.\n" +
                        "* Basic editing can be done using lightroom. Photoshop not allowed.\n" +
                        "* You need to send both edited and unedited photos.\n\n" +
                        "How to submit:\n\n" +
                        "* Send one-one best photo on each theme (original + edited + raw) followed by above rules with the " +
                        "note containing\n" +
                        "* Name of College\n" +
                        "* Caption\n" +
                        "* Email id (the one which you used to register for Version) \n" +
                        "* Contact number\n" +
                        "* Tools used in Light room\n" +
                        "* Submit your entries at ppcmca2018@gmail.com\n" +
                        "Photos will be posted on Version, NIT Trichy Facebook page.\n" +
                        "Judgement will be based on likes and the decision of Jury\n\n\n");
                des.setArguments(bundle);
                break;

        }
        FragmentTransaction t = getFragmentManager().beginTransaction();
        t.replace(R.id.frame,des);
        t.addToBackStack("events1");
        t.commit();
    }
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
